#!/bin/bash

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

[ "$(whoami)" == "root" ] || { echo "${ERRORPREFIX}This script must be run with 'sudo'." >&2; exit 1; }

rm installation.log
touch installation.log

printf "Installing project dependencies:\n"

install() {
    printf "[${YELLOW}INSTALLING${NC}]  %s" $1
    apt-get -y install $2 >> installation.log 2>> installation.log
    if [ $? -eq 0 ]; then
        printf "\r[${GREEN}    OK    ${NC}]  %s\n" $1
    else
        printf "\r[${RED}  FAILED  ${NC}]  %s\nInstallation ${RED}failed${NC}. See ${RED}installation.log${NC} for details\n" $1
        exit 1;
    fi
}

install "g++" g++
install "CMake" cmake
install "Qt" qt-sdk

printf "Installation ${GREEN}succeeded${NC}.\n"
